import os
import dill
import pickle
import numpy as np
from matplotlib import pyplot as plt
import math #needed for definition of pi
from xpresso.ai.core.utils.jupyter_experiment_utils import xpresso_save_plot
from xpresso.ai.core.commons.exceptions.xpr_exceptions import MountPathNoneType


if not os.environ["XPRESSO_MOUNT_PATH"]:
    raise MountPathNoneType
PICKLE_PATH = os.path.join(os.environ["XPRESSO_MOUNT_PATH"], "xjp_store")

if not os.path.exists(PICKLE_PATH):
    os.makedirs(PICKLE_PATH, mode=0o777, exist_ok=True)

try:
    MOUNT_PATH = pickle.load(open(f"{PICKLE_PATH}/MOUNT_PATH.pkl", "rb"))
except (NameError, FileNotFoundError) as e:
    print(str(e))

## $xpr_param_component_name = component_a
## $xpr_param_component_type = pipeline_job
## $xpr_param_global_variables = ["MOUNT_PATH"]

# This is the same path defined as mount_path during pipeline deployment
MOUNT_PATH = os.environ['XPRESSO_MOUNT_PATH']


x = np.arange(0, math.pi*2, 0.05)
y = np.sin(x)
plt.plot(x,y)
plt.xlabel("angle")
plt.ylabel("sine")
plt.title('sine wave')

# Target method
xpresso_save_plot("image_1", output_path=os.environ['XPRESSO_MOUNT_PATH'],output_folder="xjp_images/component_a")

try:
    pickle.dump(MOUNT_PATH, open(f"{PICKLE_PATH}/MOUNT_PATH.pkl", "wb"))
except (NameError, FileNotFoundError) as e:
    print(str(e))

