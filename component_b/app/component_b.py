import os
import dill
import pickle
import numpy as np
from matplotlib import pyplot as plt
import math #needed for definition of pi
from xpresso.ai.core.utils.jupyter_experiment_utils import xpresso_save_plot
from xpresso.ai.core.commons.exceptions.xpr_exceptions import MountPathNoneType
## $xpr_param_component_name = component_b
## $xpr_param_component_type = job
## $xpr_param_global_variables = ["MOUNT_PATH"]

# This is the same path defined as mount_path during pipeline deployment
MOUNT_PATH = os.environ['XPRESSO_MOUNT_PATH']


x = np.arange(0, math.pi*2, 0.05)
y = np.sin(x)
plt.plot(x,y)
plt.xlabel("angle")
plt.ylabel("sine")
plt.title('sine wave')

# Target method
xpresso_save_plot("image_1", output_path=os.environ['XPRESSO_MOUNT_PATH'],output_folder="xjp_images/component_b")
